import pygame
import subs

nrows = 15
ncolumns = 20
window_size = (1200, 750)
window_game_size = (900, 750)

# инициализируем библиотеку Pygame
pygame.init()

# задаем название окна
pygame.display.set_caption("Сетка")

# создаем окно
window = pygame.display.set_mode(window_size)

subs.Field(window, nrows, ncolumns, window_size, window_game_size)
