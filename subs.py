import pygame
import time

class Clrs():
    white = (255, 255, 255)
    black = (0, 0, 0)
    lgray = (200, 200, 200)
    gray = (100, 100, 100)

class Fnts():
    def render(st, col, sz):
        return pygame.font.SysFont('Verdana', sz).render(st, True, col)

class Node():
    def __init__(self):
        self.type = -1

class Field():
    def __init__(self, window, nrows, ncolumns, window_size, window_game_size):
        self.nrows = nrows
        self.ncolumns = ncolumns
        self.wsize = window_size
        self.wgsize = window_game_size
        self.unit = (self.wgsize[0]/self.ncolumns, self.wgsize[1]/self.nrows)
        self.field = [[Node() for j in range(ncolumns)] for i in range(nrows)]
        print(len(self.field), nrows)
        self.drawer = Drawer(window, nrows, ncolumns, window_size, window_game_size, self.unit)
        self.start_updating()

    def update(self, row, col):
        self.field[row-1][col-1].type *= -1

    def start_updating(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    exit
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    self.last_click = time.time()
                elif event.type == pygame.MOUSEBUTTONUP and time.time()-self.last_click<0.3:
                    self.drawer.draw()
                    if event.pos[0]<self.wgsize[0] and event.pos[1]<self.wgsize[1]:
                        row = int(event.pos[1] // self.unit[1])+1
                        col = int(event.pos[0] // self.unit[0])+1
                        self.update(row, col)
                        self.drawer.draw_choosen(row, col)
                self.drawer.draw_field(self.field)
                self.drawer.update()
        

class Drawer():
    def __init__(self, window, nrows, ncolumns, window_size, window_game_size, unit):
        self.window = window
        self.nrows = nrows
        self.ncolumns = ncolumns
        self.wsize = window_size
        self.wgsize = window_game_size
        self.unit = unit
        self.is_start = False
        self.draw()
    
    def draw(self):
        self.window.fill(Clrs.white)
        pygame.draw.line(self.window, Clrs.gray, (0, 0), (self.wgsize[0], 0), 1)
        pygame.draw.line(self.window, Clrs.gray, (self.wgsize[0], 0), (self.wgsize[0], self.wgsize[1]-1), 1)
        pygame.draw.line(self.window, Clrs.gray, (self.wgsize[0], self.wgsize[1]-1), (0, self.wgsize[1]-1), 1)
        pygame.draw.line(self.window, Clrs.gray, (0, self.wgsize[1]-1), (0, 0), 1)
        for i in range(1, self.ncolumns):
            pygame.draw.line(self.window, Clrs.lgray, (i*self.unit[0], 0), (i*self.unit[0], self.wgsize[1]), 2)

        for i in range(1, self.nrows):
            pygame.draw.line(self.window, Clrs.lgray, (0, i*self.unit[1]), (self.wgsize[0], i*self.unit[1]), 2)

        info_text = Fnts.render('Информация', Clrs.black, 22)
        text_pos = self.get_text_pos(info_text, 0, 100)
        self.window.blit(info_text, text_pos)

        if self.is_start:
            self.draw_button('Пауза', 18, self.wsize[1]-105, self.wsize[1]-55)
            self.draw_button('Очистить', 18, self.wsize[1]-50, self.wsize[1])
        else:
            self.draw_button('Начать', 18, self.wsize[1]-105, self.wsize[1]-55)
            self.draw_button('Очистить', 18, self.wsize[1]-50, self.wsize[1])

    def draw_button(self, text, sz, start, end):
        button_rect = pygame.Rect(self.wgsize[0], start, self.wsize[0]-self.wgsize[0], end-start)
        pygame.draw.rect(self.window, Clrs.lgray, button_rect)
        button_text = Fnts.render(text, Clrs.black, sz)
        text_pos = self.get_text_pos(button_text, start, end)
        self.window.blit(button_text, text_pos)

    def get_text_pos(self, text, start, end):
        return text.get_rect(center=pygame.Rect(self.wgsize[0], start, self.wsize[0]-self.wgsize[0], end-start).center)

    def draw_choosen(self, row, col):
        up = (row-1)*self.unit[1]+1
        left = (col-1)*self.unit[0]+1
        down = row*self.unit[1]
        right = col*self.unit[0]
        pygame.draw.line(self.window, Clrs.gray, (left, up), (right, up), 1)
        pygame.draw.line(self.window, Clrs.gray, (right, up), (right, down), 1)
        pygame.draw.line(self.window, Clrs.gray, (right, down), (left, down), 1)
        pygame.draw.line(self.window, Clrs.gray, (left, down), (left, up), 1)
        choosed_text = Fnts.render(f'Вы выбрали клетку {row}:{col}', Clrs.black, 12)   
        textpos = self.get_text_pos(choosed_text, 500, 20)
        self.window.blit(choosed_text, textpos)

    def draw_field(self, field):
        for i in range(self.nrows):
            for j in range(self.ncolumns):
                if field[i][j].type == 1:
                    pygame.draw.rect(self.window, Clrs.black, (j*self.unit[0]+1, i*self.unit[1]+1, self.unit[0], self.unit[1]))

    def set_is_start(self, is_start):
        self.is_start = is_start

    def update(self):
        pygame.display.flip()

    
